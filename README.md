# neatlist (a guided wrapper for Void Linux services)

neatlist is a guided wrapper for Void Linux services. The services will be presented and actions will be offered.

### Fixes + added features

2023-09-13
- feature added: detecting orphaned symlinks in /var/service

### Dependencies

### How to use

Download:

`git clone https://gitlab.com/Irkiosan/voidlinux-services-wrapper.git`

(Optional) Rename the script:

`mv ./voidlinux-services-wrapper/neatlist ./voidlinux-services-wrapper/<YourDesiredName>` Please make sure to replace 'neatlist' in the following commands with your chosen name.

Make the scripts executable:

`chmod +x ./voidlinux-services-wrapper/neatlist`

Move the script to a folder in your $PATH (e.g. /usr/bin):

`sudo mv ./voidlinux-services-wrapper/neatlist /usr/bin`

Remove the downloaded files:

`rm -rf ./voidlinux-services-wrapper/`

Close and re-open the terminal.

### Features

- Enter `neatlist` in your terminal.
- You are presented with a list of your services (according to /etc/sv and /var/service). The list shows the service name, and it's current status
- Choose a service
- Choose an action, e.g. enable (symlinks the service), disable (rm the symlink), up, down, restart, status... according to the status of the service
